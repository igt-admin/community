package com.community.common.core.domain;



import lombok.Data;

import java.io.Serializable;

/**
 * 地理坐标
 */
@Data
public class Point implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 经度 */
    private Double longitude;
    /** 维度 */
    private Double latitude;

//    public Point(Double latitude, Double longitude) {
//        this.latitude = latitude;
//        this.longitude = longitude;
//    }
//
//    @Override
//    public String toString() {
////        'POINT(1 1)'
//        return "POINT(" + latitude + " " + longitude + ')';
//    }
}
