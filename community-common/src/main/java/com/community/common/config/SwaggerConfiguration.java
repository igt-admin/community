package com.community.common.config;


import io.swagger.annotations.Api;
import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.*;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname SwaggerConfiguration
 * @Description TODO
 * @Date 2021/1/30 21:07
 * @Author liuxiaobin
 * @Version 1.0
 **/
@Component
@EnableOpenApi
@Data
public class SwaggerConfiguration {
    @Bean
    public Docket webApiDoc() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("IGT后端接口文档")
                .pathMapping("/")
                // 定义是否开启swagger，false为关闭，可以通过变量控制，线上关闭
                .enable(true)
                //配置api文档元信息
                .apiInfo(apiInfo())
                // 选择哪些接口作为swagger的doc发布
                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.community"))
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                //正则匹配请求路径，并分配至当前分组
                .paths(PathSelectors.any())
                .build()
                //新版swagger3.0配置
                .globalRequestParameters(getGlobalRequestParameters())
                .globalResponses(HttpMethod.GET, getGlobalResponseMessage())
                .globalResponses(HttpMethod.POST, getGlobalResponseMessage());

    }

    @Bean
    public Docket adminApiDoc() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("管理端接口文档")
                .pathMapping("/")
                // 定义是否开启swagger，false为关闭，可以通过变量控制，线上关闭
                .enable(true)
                //配置api文档元信息
                .apiInfo(apiInfo())
                // 选择哪些接口作为swagger的doc发布
                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.community"))
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                //正则匹配请求路径，并分配至当前分组
                .paths(PathSelectors.ant("/community/**"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("社区信息平台")
                .description("接口文档()")
                //TODO 添加多个项目跳转url路径
                .contact(new Contact("社区信息", "http://www.baidu.com", "123@qq.com"))
                .contact(new Contact("排序参数 url?orderByColumn=module&isAsc=desc", "", "123@qq.com"))
                .version("12")
                .build();
    }

    /**
     * 生成全局通用参数, 支持配置多个响应参数
     *
     * @return
     */
    private List<RequestParameter> getGlobalRequestParameters() {
        List<RequestParameter> parameters = new ArrayList<>();
//        parameters.add(new RequestParameterBuilder()
//                .name("appKey")
//                .description("appKey")
//                .in(ParameterType.HEADER)
//                .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
//                .required(true)
//                .build());
//        parameters.add(new RequestParameterBuilder()
//                .name("sign")
//                .description("签名")
//                .in(ParameterType.HEADER)
//                .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
//                .required(true)
//                .build());
        return parameters;
    }

    /**
     * 生成通用响应信息
     *
     * @return
     */
    private List<Response> getGlobalResponseMessage() {
        List<Response> responseList = new ArrayList<>();
        responseList.add(new ResponseBuilder().code("4xx").description("请求错误，根据code和msg检查").build());
        return responseList;
    }


}
