package com.community.common.enums;

/**
 * 数据源
 *
 * @author community
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
