package com.community.web.controller.system;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.community.common.utils.SecurityUtils;
import com.community.common.utils.StringUtils;
import com.community.system.domain.request.CommunityMessageRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.community.common.annotation.Log;
import com.community.common.core.controller.BaseController;
import com.community.common.core.domain.AjaxResult;
import com.community.common.enums.BusinessType;
import com.community.system.domain.CommunityMessage;
import com.community.system.service.ICommunityMessageService;
import com.community.common.utils.poi.ExcelUtil;
import com.community.common.core.page.TableDataInfo;

/**
 * 社区发布信息Controller
 *
 * @author community
 * @date 2024-06-01
 */
@RestController
@RequestMapping("/community/message")
@Api( tags ="社区发布信息接口")
public class CommunityMessageController extends BaseController
{
    @Autowired
    private ICommunityMessageService communityMessageService;

    /**
     * 查询社区发布信息列表
     */
    //@PreAuthorize("@ss.hasPermi('system:message:list')")
    @ApiOperation("获取社区信息列表(首页列表/待审核列表)")
    @GetMapping("/list")
    public TableDataInfo list(CommunityMessageRequest request)
    {
        startPage();
        List<CommunityMessage> list = communityMessageService.selectCommunityMessageList(request);
        return getDataTable(list);
    }

    /**
     * 消息帖子详情
     */
    //@PreAuthorize("@ss.hasPermi('system:message:list')")
    @ApiOperation("获取消息帖子详情")
    @GetMapping("/getById/{id}")
    public AjaxResult getById(@PathVariable("id") Long id)
    {
        return AjaxResult.success(communityMessageService.selectCommunityMessageByMessageId(id));
    }


    /**
     * 新增社区发布信息
     */
    //@PreAuthorize("@ss.hasPermi('system:message:add')")
    @Log(title = "社区发布信息新增", businessType = BusinessType.INSERT)
    @ApiOperation("社区发布信息新增")
    @PostMapping
    public AjaxResult add(@RequestBody CommunityMessage communityMessage)
    {
        return toAjax(communityMessageService.insertCommunityMessage(communityMessage));
    }

    /**
     * 修改社区发布信息
     */
    //@PreAuthorize("@ss.hasPermi('system:message:edit')")
    @Log(title = "社区发布信息", businessType = BusinessType.UPDATE)
    @ApiOperation("社区发布信息修改")
    @PutMapping
    public AjaxResult edit(@RequestBody CommunityMessage communityMessage)
    {
        return toAjax(communityMessageService.updateCommunityMessage(communityMessage));
    }

    /**
     * 删除社区发布信息
     */
    //@PreAuthorize("@ss.hasPermi('system:message:remove')")
    @Log(title = "社区发布信息删除", businessType = BusinessType.DELETE)
    @ApiOperation("社区发布信息删除(多个id逗号拼接)")
	@DeleteMapping("/{messageIds}")
    public AjaxResult remove(@PathVariable Long[] messageIds)
    {
        return toAjax(communityMessageService.deleteCommunityMessageByMessageIds(messageIds));
    }


    @ApiOperation("我的-获取历史发布列表")
    @GetMapping("/myList")
    public TableDataInfo myList(){
        startPage();
        CommunityMessageRequest request = new CommunityMessageRequest();
        request.setPromulgatorPhonenumber(SecurityUtils.getLoginUser().getUser().getPhonenumber());
        List<CommunityMessage> list = communityMessageService.selectCommunityMessageList(request);
        return getDataTable(list);
    }

    @ApiOperation("审批")
    @Log(title = "管理员审批", businessType = BusinessType.UPDATE)
    @PostMapping("/audit")
    public AjaxResult audit(@RequestBody CommunityMessage communityMessage){
        if(communityMessage.getMessageId()!=null || communityMessage.getStatus()==null ){
            AjaxResult.error("消息id或审批状态不能为空");
        }
        // 判断数据库中状态为可审批
        CommunityMessage message = communityMessageService.selectCommunityMessageByMessageId(communityMessage.getMessageId());
        if(message.getStatus() == 1){
            // 更新审批状态
            message.setStatus(communityMessage.getStatus());
            message.setUpdateTime(new Date());
            return toAjax(communityMessageService.updateCommunityMessage(message));
        }
        return AjaxResult.error("已审批，请勿重复操作");
    }


    @ApiOperation("置顶")
    @Log(title = "管理员", businessType = BusinessType.UPDATE)
    @PostMapping("/pinToTop")
    public AjaxResult pinToTop(@RequestBody CommunityMessage communityMessage){
        if(communityMessage.getMessageId()!=null || communityMessage.getIsTop()==null ){
            AjaxResult.error("消息id或置顶类型不能为空");
        }
        // 判断数据库中状态为可审批
        CommunityMessage message = communityMessageService.selectCommunityMessageByMessageId(communityMessage.getMessageId());
        message.setIsTop(communityMessage.getIsTop());
        message.setUpdateTime(new Date());
        return toAjax(communityMessageService.updateCommunityMessage(message));
    }


}
