package com.community.system.service.impl;

import java.util.List;
import com.community.common.utils.DateUtils;
import com.community.common.utils.StringUtils;
import com.community.system.domain.request.CommunityMessageRequest;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.community.system.mapper.CommunityMessageMapper;
import com.community.system.domain.CommunityMessage;
import com.community.system.service.ICommunityMessageService;

/**
 * 社区发布信息Service业务层处理
 *
 * @author community
 * @date 2024-06-01
 */
@Service
public class CommunityMessageServiceImpl implements ICommunityMessageService
{
    @Autowired
    private CommunityMessageMapper communityMessageMapper;

    /**
     * 查询社区发布信息
     *
     * @param messageId 社区发布信息主键
     * @return 社区发布信息
     */
    @Override
    public CommunityMessage selectCommunityMessageByMessageId(Long messageId)
    {
        return communityMessageMapper.selectCommunityMessageByMessageId(messageId);
    }

    /**
     * 查询社区发布信息列表
     *
     * @param communityMessage 社区发布信息
     * @return 社区发布信息
     */
    @Override
    public List<CommunityMessage> selectCommunityMessageList(CommunityMessageRequest communityMessage)
    {
        return communityMessageMapper.selectCommunityMessageList(communityMessage);
    }

    /**
     * 新增社区发布信息
     *
     * @param communityMessage 社区发布信息
     * @return 结果
     */
    @Override
    public int insertCommunityMessage(CommunityMessage communityMessage)
    {
        communityMessage.setCreateTime(DateUtils.getNowDate());
        // 默认置顶为否
        communityMessage.setIsTop(0);
        if(StringUtils.isBlank(communityMessage.getTag())){
            communityMessage.setTag(communityMessage.getModule());
        }
        return communityMessageMapper.insertCommunityMessage(communityMessage);
    }

    /**
     * 修改社区发布信息
     *
     * @param communityMessage 社区发布信息
     * @return 结果
     */
    @Override
    public int updateCommunityMessage(CommunityMessage communityMessage)
    {
        communityMessage.setUpdateTime(DateUtils.getNowDate());
        return communityMessageMapper.updateCommunityMessage(communityMessage);
    }

    /**
     * 批量删除社区发布信息
     *
     * @param messageIds 需要删除的社区发布信息主键
     * @return 结果
     */
    @Override
    public int deleteCommunityMessageByMessageIds(Long[] messageIds)
    {
        return communityMessageMapper.deleteCommunityMessageByMessageIds(messageIds);
    }

    /**
     * 删除社区发布信息信息
     *
     * @param messageId 社区发布信息主键
     * @return 结果
     */
    @Override
    public int deleteCommunityMessageByMessageId(Long messageId)
    {
        return communityMessageMapper.deleteCommunityMessageByMessageId(messageId);
    }
}
