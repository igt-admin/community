package com.community.system.service;

import java.util.List;
import com.community.system.domain.CommunityMessage;
import com.community.system.domain.request.CommunityMessageRequest;

/**
 * 社区发布信息Service接口
 *
 * @author community
 * @date 2024-06-01
 */
public interface ICommunityMessageService
{
    /**
     * 查询社区发布信息
     *
     * @param messageId 社区发布信息主键
     * @return 社区发布信息
     */
    public CommunityMessage selectCommunityMessageByMessageId(Long messageId);

    /**
     * 查询社区发布信息列表
     *
     * @param communityMessage 社区发布信息
     * @return 社区发布信息集合
     */
    public List<CommunityMessage> selectCommunityMessageList(CommunityMessageRequest communityMessage);

    /**
     * 新增社区发布信息
     *
     * @param communityMessage 社区发布信息
     * @return 结果
     */
    public int insertCommunityMessage(CommunityMessage communityMessage);

    /**
     * 修改社区发布信息
     *
     * @param communityMessage 社区发布信息
     * @return 结果
     */
    public int updateCommunityMessage(CommunityMessage communityMessage);

    /**
     * 批量删除社区发布信息
     *
     * @param messageIds 需要删除的社区发布信息主键集合
     * @return 结果
     */
    public int deleteCommunityMessageByMessageIds(Long[] messageIds);

    /**
     * 删除社区发布信息信息
     *
     * @param messageId 社区发布信息主键
     * @return 结果
     */
    public int deleteCommunityMessageByMessageId(Long messageId);

}
