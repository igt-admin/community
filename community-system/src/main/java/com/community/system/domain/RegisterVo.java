package com.community.system.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:
 * @Author: chenwy
 * @Created: 2024-06-16
 */
@Data
public class RegisterVo {

    @ApiModelProperty(value = "登录名称", name = "userName",required = false, example = "1")
    private String userName;

    @ApiModelProperty(value = "密码", name = "password",required = false, example = "1")
    private String password;
}
