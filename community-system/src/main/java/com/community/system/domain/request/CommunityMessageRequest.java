package com.community.system.domain.request;

import com.community.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 社区发布信息对象 community_message
 *
 * @author community
 * @date 2024-06-01
 */
@Data
@ApiModel(value="CommunityMessageRequest 对象", description="社区帖子信息查询对象")
public class CommunityMessageRequest implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** keyword 关键字搜索(标题,内容,标签) */
    @ApiModelProperty(value = "关键字搜索(标题,内容,标签)", name = "keyword",required = false, example = "政务")
    private String keyword;
    /** 模块 */
    @ApiModelProperty(value = "模块", name = "module",required = false, example = "政务")
    private String module;

    /** 状态(1:待审核 2:审核不通过(可重编辑)  3:审核通过 */
    @Excel(name = "状态(1:待审核 (管理审核列表) 2:审核不通过(可重编辑)  3:审核通过(首页展示)")
    @ApiModelProperty(value = "状态(1:待审核 2:审核不通过(可重编辑)  3:审核通过", name = "status",required = false, example = "1")
    private Integer status;

    /** 经纬度(经度) */
    @ApiModelProperty(value = "经纬度(经度)", name = "longitude",required = false, example = "1.001")
    private Double longitude;

    /** 经纬度(维度) */
    @ApiModelProperty(value = "经纬度(维度)", name = "latitude",required = false, example = "1.002")
    private Double latitude;

    /** 经纬度(维度) */
    @ApiModelProperty(value = "发布人手机号（后端使用）", name = "promulgatorPhonenumber",required = false, example = "1.002")
    private String promulgatorPhonenumber;





}
