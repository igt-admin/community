package com.community.system.domain;

import com.community.common.annotation.Excel;
import com.community.common.core.domain.BaseEntity;

import com.community.common.core.domain.Point;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * 社区发布信息对象 community_message
 *
 * @author community
 * @date 2024-06-01
 */
@Data
@ApiModel(value="CommunityMessage对象", description="社区帖子信息对象")
public class CommunityMessage implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 消息帖子id */
    @ApiModelProperty(value = "消息帖子id", name = "messageId",required = false, example = "1")
    private Long messageId;

    /** 六大模块 */
    @Excel(name = "六大模块")
    @ApiModelProperty(value = "六大模块", name = "module",required = false, example = "1")
    private String module;

    /** 标签(用户输入定义 不填默认模块) */
    @Excel(name = "标签(用户输入定义 不填默认为模块)")
    @ApiModelProperty(value = "标签(用户输入定义 不填默认为模块)", name = "tag",required = false, example = "1")
    private String tag;

    /** 标题 */
    @Excel(name = "标题")
    @ApiModelProperty(value = "标题", name = "title",required = false, example = "1")
    private String title;

    /** 内容 */
    @Excel(name = "内容")
    @ApiModelProperty(value = "内容", name = "content",required = false, example = "1")
    private String content;


    /** 保留天数默认3天 (3/7/15/30) */
    @Excel(name = "保留天数默认3天 (3/7/15/30)")
    @ApiModelProperty(value = "保留天数默认3天 (3/7/15/30)", name = "retentionDays",required = false, example = "1")
    private Integer retentionDays;

    /** 发布者手机号 */
    @Excel(name = "发布者手机号")
    @ApiModelProperty(value = "发布者手机号", name = "promulgatorPhonenumber",required = false, example = "1")
    private String promulgatorPhonenumber;

    /** 状态(1:待审核 2:审核不通过(可重编辑)  3:审核通过 */
    @Excel(name = "状态(1:待审核 2:审核不通过(可重编辑)  3:审核通过")
    @ApiModelProperty(value = "状态(1:待审核 2:审核不通过(可重编辑)  3:审核通过", name = "status",required = false, example = "1")
    private Integer status;

    /** 经纬度(经度) */
    @ApiModelProperty(value = "经纬度(经度)", name = "longitude",required = false, example = "1.001")
    private Double longitude;
    /** 经纬度(维度) */
    @ApiModelProperty(value = "经纬度(维度)", name = "latitude",required = false, example = "1.002")
    private Double latitude;

    /** 距离(米) */
    @ApiModelProperty(value = "距离(米) ", name = "distance",required = false, example = "1")
    private Integer distance;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /** 地址 */
    @ApiModelProperty(value = "地址 ", name = "address",required = false, example = "深圳市宝安区新安街道")
    private String address;
    /** 联系方式 */
    @ApiModelProperty(value = "联系方式 ", name = "contactInformation",required = false, example = "15320710022")
    private String contactInformation;
    /** 昵称 */
    @ApiModelProperty(value = "昵称 ", name = "nickName",required = false, example = "张三")
    private String nickName;

    @ApiModelProperty(value = "是否置顶 0否 1是 ", name = "isTop",required = false, example = "张三")
    private Integer isTop;






}
