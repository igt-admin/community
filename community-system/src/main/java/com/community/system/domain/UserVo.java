package com.community.system.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:
 * @Author: chenwy
 * @Created: 2024-08-02
 */
@Data
public class UserVo {

    @ApiModelProperty(value = "昵称", name = "userId",required = true, example = "1")
    private Long userId;

    @ApiModelProperty(value = "昵称", name = "nickName",required = false, example = "1")
    private String nickName;

    @ApiModelProperty(value = "密码", name = "password",required = false, example = "1")
    private String password;


}
